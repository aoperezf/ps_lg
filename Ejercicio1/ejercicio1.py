#!/usr/bin/python
# -*- coding: utf-8 -*- 
#------------------------------------------------------------------------------
# Copyright (C) 2014 by aoperez. All Rights Reserved.
# Author: Alberto Ocanya
# Contact: aoperez@gmail.com
#------------------------------------------------------------------------------
str_app_name = "Ejercicio1"
str_app_version = "1.0"

try:
	import sys
	import os.path
	

except:
	print "ERR: Import"

# debug
bool_debug = True

#----------------------------------------------------------
# AppUsage
#----------------------------------------------------------
def AppUsage ():
	print "--------------------------------------------------------------------------------"
	print " "+str_app_name+" v"+str_app_version+" - (c)aoperez"
	print "--------------------------------------------------------------------------------"
	print " Description:"
	print "   Comprueba si una lista de números es perfecto, divisible...."
	print " Sintaxis:"
	print "   ejercicio1.py <lista>"
	print " Parametros:"
	print "   <lista>       - lista de números a comprobar x,y,z,...."
	print " Ejemplos:"
	print "   ejercicio1.py [12,25,31]"
	print "--------------------------------------------------------------------------------"


#-----------------------------------------------
# funciones numericas
#-----------------------------------------------

def isAbundant(n):
	sumatorio=0
	for i in range(1,n):
	    if n%i==0:
	        sumatorio=sumatorio+i
    
	if sumatorio>n:
	    return True

def isDefective(n):
	sumatorio=0
	for i in range(1,n):
	    if n%i==0:
	        sumatorio=sumatorio+i
    
	if sumatorio<n:
	    return True

def isPerfect(n):
	sumatorio=0
	lista=[]
	for i in range(1,n):
	    if n%i==0:
	        sumatorio=sumatorio+i
	        lista.append(i)
 
    
	if sumatorio==n:
	    return True



#------------------------------------------------
# main
#------------------------------------------------
def main():

	# csv file
	if len(sys.argv) > 1:
		lista = sys.argv[1].split(",")

		if len(lista) <= 0:
			AppUsage()
			sys.exit(0)
	else:
		AppUsage()
		sys.exit(0)

	print "INFO: %s" % ("App Begin ----------------------------------------------------------------")
	print "INFO: %s v%s" % (str_app_name, str_app_version)

	#print "Lista: [%s]" % (numero)
	print "Lista:"
	print lista


	listaResultados = []
	for numero in lista:
		valor = int(numero)
		if isPerfect(valor):
			listaResultados.append("Perfecto")
		if isAbundant(valor):
			listaResultados.append("Abundante")
		if isDefective(valor):
			listaResultados.append("Defectivo")


	print "Resultados:"
	for i in range(len(lista)):
		print "número: " + lista[i] + " es " + listaResultados[i]


	print "INFO: %s" % ("Dump End ------------------------------------------------------------------")
	print "INFO: %s" % ("App End ------------------------------------------------------------------")

#------------------------------------------------
# Entry point
#------------------------------------------------
if __name__ == "__main__":
	main ()