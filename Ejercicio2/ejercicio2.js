var url1 = "http://s3.amazonaws.com/logtrust-static/test/test/data1.json";
var url2 = "http://s3.amazonaws.com/logtrust-static/test/test/data2.json";
var url3 = "http://s3.amazonaws.com/logtrust-static/test/test/data3.json";


/* Funciones auxiliares */
function inArray(needle, haystack) {
    var length = haystack.length;
    for (var i = 0; i < length; i++) {
        if (haystack[i] == needle) return true;
    }
    return false;
}

function devolverCategoria(cadena) {

    var categoria = "";

    var indice1 = cadena.indexOf("#");

    var subcadena = cadena.substr(indice1 + 1, cadena.length);
    var indice2 = subcadena.indexOf("#")

    categoria = subcadena.substr(0, indice2);
    return categoria;
}

function devolverFecha(cadena) {
    var fecha = "";

    var indice = cadena.indexOf("-");
    var indice2 = cadena.lastIndexOf("-");
    fecha = cadena.substring(indice - 4, indice2 + 3);

    return fecha;
}

function procesarDatosX(lista, titulo) {
    datosX = [];

    switch (titulo) {
        case "Datos1":
            for (var i = 0; i < lista.length; i++) {
                lista[i].cat = lista[i].cat.toUpperCase();

                if (!inArray(lista[i].d, datosX)) {

                    datosX.push(lista[i].d);
                }
            }

            return datosX.sort(function(a, b) {
                return a - b });

            break;
        case "Datos2":
            for (var i = 0; i < lista.length; i++) {
                lista[i].categ = lista[i].categ.toUpperCase();

                if (!inArray(lista[i].myDate, datosX)) {

                    datosX.push(lista[i].myDate);
                }
            }

            return datosX.sort(function(item1, item2) {
                if (item1 == item2)
                    return 0;
                if (item1 > item2)
                    return 1;
                if (item1 < item2)
                    return -1;
            });
            break;
        case "Datos3":

            for (var i = 0; i < lista.length; i++) {
                var cadena = lista[i].raw;
                var categoria = devolverCategoria(cadena);
                var fecha = devolverFecha(cadena);
                lista[i].categ = categoria.toUpperCase();
                lista[i].myDate = fecha;

                if (!inArray(fecha, datosX)) {

                    datosX.push(fecha);
                }
            }

            return datosX.sort(function(item1, item2) {
                if (item1 == item2)
                    return 0;
                if (item1 > item2)
                    return 1;
                if (item1 < item2)
                    return -1;
            });
            break;
    }



}

function procesarDatosCategorias(lista, titulo) {
    datosX = [];

    switch (titulo) {
        case "Datos1":
            for (var i = 0; i < lista.length; i++) {

                if (!inArray(lista[i].cat, datosX)) {

                    datosX.push(lista[i].cat);
                }
            }

            return datosX.sort(function(a, b) {
                return a - b });

            break;
        case "Datos2":
            for (var i = 0; i < lista.length; i++) {

                if (!inArray(lista[i].categ, datosX)) {

                    datosX.push(lista[i].categ);
                }
            }

            return datosX.sort(function(a, b) {
                return a - b });

            break;

        case "Datos3":
            for (var i = 0; i < lista.length; i++) {

                if (!inArray(lista[i].categ, datosX)) {

                    datosX.push(lista[i].categ);
                }
            }

            return datosX.sort(function(a, b) {
                return a - b });

            break;
    }



}

function procesarSeries(lista, fechas, categorias, titulo) {
    var datosX = [];


    switch (titulo) {
        case "Datos1":
            for (var i = 0; i < categorias.length; i++) {
                var elementosSumas = [];
                for (var j = 0; j < fechas.length; j++) {
                    var suma = 0;
                    for (var k = 0; k < lista.length; k++) {
                        if ((lista[k].cat == categorias[i]) && (lista[k].d == fechas[j])) {
                            suma += lista[k].value;
                        }
                    }
                    elementosSumas.push(suma);

                }

                var resultado = {
                    name: categorias[i],
                    data: elementosSumas
                };

                datosX.push(resultado);
            }
            break;
        case "Datos2":
            for (var i = 0; i < categorias.length; i++) {
                var elementosSumas = [];
                for (var j = 0; j < fechas.length; j++) {
                    var suma = 0;
                    for (var k = 0; k < lista.length; k++) {
                        if ((lista[k].categ == categorias[i]) && (lista[k].myDate == fechas[j])) {
                            suma += lista[k].val;
                        }
                    }
                    elementosSumas.push(suma);

                }

                var resultado = {
                    name: categorias[i],
                    data: elementosSumas
                };

                datosX.push(resultado);
            }
            break;
        case "Datos3":
            for (var i = 0; i < categorias.length; i++) {
                var elementosSumas = [];
                for (var j = 0; j < fechas.length; j++) {
                    var suma = 0;
                    for (var k = 0; k < lista.length; k++) {
                        if ((lista[k].categ == categorias[i]) && (lista[k].myDate == fechas[j])) {
                            suma += lista[k].val;
                        }
                    }
                    elementosSumas.push(suma);

                }

                var resultado = {
                    name: categorias[i],
                    data: elementosSumas
                };

                datosX.push(resultado);
            }
            break;
    }


    return datosX;
}

function procesarFechasEje(lista) {
    var resultado = [];

    for (var i = 0; i < lista.length; i++) {
        var fecha = new Date(lista[i]);
        resultado.push(fecha.toLocaleDateString());
    }

    return resultado;
}

function devolverDatosPie(datos) {

    var lista = [];

    for (var i = 0; i < datos.length; i++) {
        var sumatorio = 0;
        for (var j = 0; j < datos[i].data.length; j++) {
            sumatorio += datos[i].data[j];
        }
        var objeto = {
            name: datos[i].name,
            y: (sumatorio / 100)
        };

        lista.push(objeto);
    }


    return lista;
}

/* Funciones gráficas */
function pintarGraficasLineas() {
    graficaLineas("Datos1", url1);
    graficaLineas("Datos2", url2);
    graficaLineas("Datos3", url3);
}

//esta función se trae los datos del servidor y pinta la gráfica de líneas
//ademas llamará a la función de gráfico de porcentajes
function graficaLineas(titulo, url) {

    var datos, series, fechas, categorias;

    var xhr = new XMLHttpRequest();

    //primero hay que sacar los datos mediante llamada al servicio de datos
    xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            datos = JSON.parse(this.responseText);

            //procesamos los datos

            fechas = procesarDatosX(datos, titulo);
            fechasEje = procesarFechasEje(fechas);
            categorias = procesarDatosCategorias(datos, titulo);
            series = procesarSeries(datos, fechas, categorias, titulo);

            //ahora se pinta la gráfica de líneas
            $(function() {
                Highcharts.chart(titulo, {
                    title: {
                        text: 'Ejercicio LogTrust 2 ' + titulo,
                        x: -20 //center
                    },
                    subtitle: {
                        text: 'Alberto Ocaña',
                        x: -20
                    },
                    xAxis: {
                        categories: fechasEje
                    },
                    yAxis: {
                        title: {
                            text: 'Valores'
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
                    tooltip: {
                        valueSuffix: ''
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    series: series
                });
            });


            //se hace la llamada para pintar la gráfica de porcentajes

            var datosPie = devolverDatosPie(series);
            pieChart(titulo, datosPie);
        }
    };

    //se llama al servicio de datos
    xhr.open("GET", url, true);
    xhr.send();
}

//funcion que se utiliza para pintar la gráfica de porcentajes
function pieChart(titulo, datos) {
    $(function() {
        Highcharts.chart(titulo + 'pie', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: titulo + ' Pie Chart'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                name: 'Categorías',
                colorByPoint: true,
                data: datos
            }]
        });
    });
}
